<!doctype html>
<html>
<head>
  <title>TS-N.NET Channeldeleter - Crawl Date</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="style.css" type="text/css">
</head>  
<body>
<?php

require_once("config.php");
require_once("ts3_lib/ts3_connect.php");

try
{
	/* connect to server, login and get TeamSpeak3_Node_Host object by URI */
	$ts3_ServerInstance = TeamSpeak3::factory("serverquery://".$cfg["user"].":".$cfg["pass"]."@".$cfg["host"].":".$cfg["query"]."/");
	$ts3_VirtualServer = TeamSpeak3::factory("serverquery://".$cfg["user"].":".$cfg["pass"]."@".$cfg["host"].":".$cfg["query"]."/?server_port=".$cfg["voice"]);
	
	require_once("ts3_lib/mysql_connect.php");
	
	$ts3_VirtualServer->selfUpdate(array('client_nickname'=>$queryname));

	echo "<table>";
	foreach($ts3_VirtualServer->channelList() as $channel)
	{
		$channelid = $channel->getId();
		$channel = $ts3_VirtualServer->channelGetById($channelid);
		$channelname = htmlspecialchars($channel);
		$userinchannel = $channel["total_clients"];
		$channelpath = $channel->getPathway();
		$datetime = date('Y-m-d H:i:s');
		$channelpath_in = addslashes($channelpath);
		
		echo "<tr>";
		echo "<td>CID ".$channelid." : </td>";
		echo "<td>".$channelname."</td>";
		$cidexists = mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM $table_channel WHERE cid='$channelid'"));
		if($cidexists[0]>0)
		{
			if($userinchannel>0)
			{
				echo "<td><span class=\"green\">The CID was found and get updated, cause there was ".$userinchannel." User inside.</span></td></tr>";
				mysql_query("UPDATE $table_channel SET lastuse='$datetime',path='$channelpath' WHERE cid='$channelid'");
			}
			else
			{
				echo "<td><span class=\"red\">The CID was found but there is no User inside, so no update!</span></td></tr>";
			}	
		}
		else
		{
			echo "<td><span class=\"blue\">There was no Entry found of the CID. It was recorded now!</span></td></tr>";
			mysql_query("INSERT INTO $table_channel (`id`,`cid`,`lastuse`,`path`) VALUES ('','$channelid','$datetime','$channelpath_in')");
		}
	
	}
	echo "</table>";
}

catch(Exception $e)
{
	echo "<span class='error'><b>Error ".$e->getCode().":</b> ".$e->getMessage()."</span>\n";
}
?>
</body>
</html>