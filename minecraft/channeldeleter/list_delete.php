<!doctype html>
<html>
<head>
  <title>TS-N.NET Channeldeleter - List next deletes</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link rel="stylesheet" href="style.css" type="text/css">
</head>  
<body>
<?php

require_once("config.php");
require_once("ts3_lib/ts3_connect.php");

try
{
	/* connect to server, login and get TeamSpeak3_Node_Host object by URI */
	$ts3_ServerInstance = TeamSpeak3::factory("serverquery://".$cfg["user"].":".$cfg["pass"]."@".$cfg["host"].":".$cfg["query"]."/");
	$ts3_VirtualServer = TeamSpeak3::factory("serverquery://".$cfg["user"].":".$cfg["pass"]."@".$cfg["host"].":".$cfg["query"]."/?server_port=".$cfg["voice"]);
	
	require_once("ts3_lib/mysql_connect.php");
	
	$ts3_VirtualServer->selfUpdate(array('client_nickname'=>$queryname));

	if (isset($_GET['sort']))
	{ $sort1 = $_GET['sort']; }
	if ($sort1=="num")
	{ $sort = "ORDER BY path"; }
	elseif ($sort1=="date")
	{ $sort = "ORDER BY lastuse"; }
	else
	{ $sort = ""; }

	if (isset($_GET['order']))
	{ $order1 = $_GET['order']; }
	if ($order1=="asc")
	{ $order = " ASC"; }
	elseif ($order1=="desc")
	{ $order = " DESC"; }
	else
	{ $order = ""; }
	$sum = $sort . $order;

	foreach($ts3_VirtualServer->channelList() as $channel)
	{
		$channelid = $channel->getId();
		$channelname = htmlspecialchars($channel);
		$userinchannel = $channel["total_clients"];
		$datetime = date('Y-m-d H:i:s');
		
		$cidexists = mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM $table_channel WHERE cid='$channelid'"));
		if($cidexists[0]>0)
		{
			mysql_query("UPDATE $table_channel SET active='1' WHERE cid='$channelid'");
		}
	}
	
	$deleteoutofdb = mysql_query("SELECT id FROM $table_channel WHERE active='0'");
	while($entry=mysql_fetch_array($deleteoutofdb, MYSQL_ASSOC))
	{
		$todeleteid = $entry['id'];
		$deletecid = mysql_fetch_array(mysql_query("SELECT cid FROM $table_channel WHERE id='$todeleteid'"));
		$deletecid = $deletecid['cid'];
		mysql_query("DELETE FROM $table_channel WHERE id='$todeleteid'");
	}
	mysql_query("UPDATE $table_channel SET active='0' WHERE active='1'");

	$todaydate = time();
	$todeletetime = $todaydate - $warntime;
	$todeletedate = date("Y-m-d H:i:s",$todeletetime);
	$count = "1";
	$deletecid = mysql_query("SELECT * FROM $table_channel $sum");
	
	echo "<table>";
	echo "<tr><th><a href=\"?sort=num&amp;order=";
	switch($order1)
		{
		case "": echo "asc"; break;
		case "asc": echo "desc"; break;
		case "desc": echo "asc";
		}
	echo "\">Pathname</a></th><th colspan=\"2\"><a href=\"?sort=date&amp;order=";
	switch($order1)
		{
		case "": echo "asc"; break;
		case "asc": echo "desc"; break;
		case "desc": echo "asc";
		}
	echo "\">Deletiontime</a></th></tr>";
	while($row = mysql_fetch_array($deletecid))
	{
		$channelid = $row['cid'];
		$channel = $ts3_VirtualServer->channelGetById($channelid);
		$datetime = strtotime($row['lastuse']);
		$test = $datetime + $unusedtime;
		$willdeletetime = strftime("%Y-%m-%d, %H:%M",$test);
		$deletiontime = $willdeletetime;
		$children = $channel->getChildren();
		$channelpath = $channel->getPathway();
		if(!in_array($channelid, $nodelete))
		{
			if($datetime<$todeletetime)
			{
				$checkspacer = $ts3_VirtualServer->channelIsSpacer($channel);
				if ($checkspacer!=1)
				{
					if($children!="")
					{
						//The Parent will delete on next run of Channeldeleter, if Children are inactive
					}
					else
					{
						mysql_query("UPDATE $table_channel SET NAMES 'utf8' SET path='$channelpath' WHERE cid='$channelid'");
						echo "<tr>";
						echo "<td>".$channel->getPathway()."</td>";
						echo "<td><span class=\"green\">The Channel would be deleted on</span><br></td>";
						echo "<td>$deletiontime</td></tr>";
						$count = $count + 1;
					}
				}
			}
		}
	}
	echo "</table>";	
	
	if($count==1)
	{
		echo "<span class=\"red\">There is nothing unused or there are not enough data at the moment.</span><br>";
	}
}

catch(Exception $e)
{
	echo "<span class='error'><b>Error ".$e->getCode().":</b> ".$e->getMessage()."</span>\n";
}
?>

</body>
</html>
