<!doctype html>
<html>
<head>
  <title>TS-N.NET Channeldeleter - Deletion</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="style.css" type="text/css">
</head>  
<body>
<?php

require_once("config.php");
require_once("ts3_lib/ts3_connect.php");

try
{
	/* connect to server, login and get TeamSpeak3_Node_Host object by URI */
	$ts3_ServerInstance = TeamSpeak3::factory("serverquery://".$cfg["user"].":".$cfg["pass"]."@".$cfg["host"].":".$cfg["query"]."/");
	$ts3_VirtualServer = TeamSpeak3::factory("serverquery://".$cfg["user"].":".$cfg["pass"]."@".$cfg["host"].":".$cfg["query"]."/?server_port=".$cfg["voice"]);
	
	require_once("ts3_lib/mysql_connect.php");
	
	$ts3_VirtualServer->selfUpdate(array('client_nickname'=>$queryname));

	foreach($ts3_VirtualServer->channelList() as $channel)
	{
		$channelid = $channel->getId();
		$cidexists = mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM $table_channel WHERE cid='$channelid'"));
		if($cidexists[0]>0)
		{
			mysql_query("UPDATE $table_channel SET active='1' WHERE cid='$channelid'");
		}
	}
	
	
	echo "<b>Clean already deleted channel out of the deletion-database...</b><br>";
	$deleteoutofdb = mysql_query("SELECT id FROM $table_channel WHERE active='0'");
	$count = "1";
	echo "<table>";
	while($entry=mysql_fetch_array($deleteoutofdb, MYSQL_ASSOC))
	{
		$todeleteid = $entry['id'];
		$deletecid = mysql_fetch_array(mysql_query("SELECT cid FROM $table_channel WHERE id='$todeleteid'"));
		$deletecid = $deletecid['cid'];
		$channelpath = mysql_fetch_array(mysql_query("SELECT path FROM $table_channel WHERE id='$todeleteid'"));
		$channelpath = $channelpath['path'];
		mysql_query("DELETE FROM $table_channel WHERE id='$todeleteid'");
		
		echo "<tr>";
		echo "<td>CID ".$deletecid." : </td>";
		echo "<td>$channelpath</td>";
		echo "<td><span class=\"green\">Channel isn't existent on TeamSpeak Server, so it was deleted out of the deletiondb.</span><br></td></tr>";
		$count = $count + 1;
	}
	echo "</table>";
	if($count==1)
	{
		echo "<span class=\"red\">There was nothing to delete yet.</span><br>";
	}
	mysql_query("UPDATE $table_channel SET active='0' WHERE active='1'");
	
	
	echo "<br><b>Clean unused channel and delete this from the server and also from the database...</b><br>";

	$todaydate = time();
	$todeletetime = $todaydate - $unusedtime;
	$todeletedate = date("Y-m-d H:i:s",$todeletetime);
	$count = "1";
	echo "All Channel, which are unused since ".$todeletedate." will/would be deleted.<br>";
	$deletecid = mysql_query("SELECT * FROM $table_channel");
	
	echo "<table>";
	while($row = mysql_fetch_array($deletecid))
	{
		$channelid = $row['cid'];
		$channel = $ts3_VirtualServer->channelGetById($channelid);
		$checkspacer = $ts3_VirtualServer->channelIsSpacer($channel);
		$datetime = strtotime($row['lastuse']);
		$children = $channel->getChildren();
		if(!in_array($channelid, $nodelete))
		{
			if($datetime<$todeletetime)
			{
				if ($checkspacer!=1)
				{
					if($children!="")
					{
						//The Parent will delete on next run of Channeldeleter, if all Childs are inactive
					}
					else
					{
						echo "<tr>";
						echo "<td>CID ".$channelid." : </td>";
						echo "<td>".$channel->getPathway()."</td>";
						$ts3_VirtualServer->channelDelete($channelid, $force=FALSE);
						echo "<td><span class=\"green\">The Channel was deleted.</span><br></td></tr>";
						$count = $count + 1;
					}
				}
			}
		}
	}
	echo "</table>";	
	
	if($count==1)
	{
		echo "<span class=\"red\">There was nothing to delete yet.</span><br>";
	}
}

catch(Exception $e)
{
	echo "<span class='error'><b>Error ".$e->getCode().":</b> ".$e->getMessage()."</span>\n";
}
?>

</body>
</html>