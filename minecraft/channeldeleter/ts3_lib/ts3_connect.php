<?php
/* set error reporting levels */
error_reporting(E_ALL | E_STRICT);

/* set default timezone */
date_default_timezone_set("Europe/Berlin");

/* load required files */
require_once("globals.php");

/* load framework library */
require_once("TeamSpeak3.php");

/* initialize */
TeamSpeak3::init();
?>