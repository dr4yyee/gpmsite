The Channeldeleter is a tool to check the activity of Channel on a TeamSpeak Server. Inactive Channel can be deleted automatically.

This tool was created from Newcomer1989 for http://www.ts-n.net.

Its allowed to use, edit and reproduce this tool but not to sell it! The Channeldeleter is a project under the GNU Generel Public License.

A description how to use it you will find on the following site. There are also a full installation guide.
http://ts-n.net/howto.php?id=79

Version: 1.01-alpha

Use this Software for your own risk! The owner does not warrant the functionality of the software. Further on, the owner not liable for damages resulting from the usage of the software.

This Software uses the PHP Framework of planetteamspeak.com, which is also a product under the GNU Generel Public License.