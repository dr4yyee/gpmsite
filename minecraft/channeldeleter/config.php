<?php
/* Fill in the TeamSpeak 3 Server Query Settings */
$cfg["host"]	= "5.45.108.117";
$cfg["query"]	= "13297";
$cfg["voice"]	= "9987";
$cfg["user"]	= "serveradmin";
$cfg["pass"]	= ".n!-@.utWqFKF1qozk@t3-xh@!p_k!h.b@V";

/* Fill in the MYSQL Database Settings */
$mysqldbhost	= "localhost";
$mysqldblogin	= "Your_MYSQL_User";
$mysqldbpasswd	= "Your_MYSQL_Password";
$mysqldbname	= "ts3_channeldeleter";
$table_channel	= "channellastuse";

/* Fill in your wishes configuration */

// The name, which the Query will use to connect
$queryname		= "ChanServ";		// its not the serverquery login name; you can name it free!

// Time, which a channel have to be unused before this will deleted
$unusedtime		= "604800";				// time in seconds; example: 604800 = 1 week

// Time, which a channel have to be unused before warning on the list_delete.php
$warntime		= "432000";				// time in seconds; example: 432000 = 5 days

// Save spacer on the server -> will not delete spacer(rooms) if this is activate
$spacer			= "1";					// 1 = active; 0 = inactive

// A list of channels (id of the channel), which should not delete automatically
$nodelete	= array(1, 2, 123, 2345);	// seperate this with a komma
?>