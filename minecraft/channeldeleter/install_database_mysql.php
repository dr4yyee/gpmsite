<!doctype html>
<html>
<head>
  <title>TS-N.NET Channeldeleter - Install MySQL Database</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="style.css" type="text/css">
</head>  
<body>
<?php

require_once("config.php");

$connect = mysql_connect($mysqldbhost, $mysqldblogin, $mysqldbpasswd) or die(mysql_error());
echo "Install Database:<br>";

$sql="CREATE DATABASE $mysqldbname";
if (mysql_query($sql,$connect))
	{
		echo "Database ".$mysqldbname." <span class=\"green\">created successfully.</span>";
	}
	else
	{
		echo "Error creating database: <span class=\"red\">".mysql_error().".</span>";
	}

echo "<br><br>Install Table(s):<br>";

$sql="CREATE TABLE $table_channel(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `lastuse` datetime NOT NULL,
  `active` int(1) NOT NULL,
  `path` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`cid`,`lastuse`)
)";

if (mysql_query("USE $mysqldbname;"))
	{
		if (mysql_query($sql,$connect))
		{
			echo "Table ".$table_channel." <span class=\"green\">created successfully.</span>";
		}
		else
		{
			echo "Error creating table: <span class=\"red\">".mysql_error().".</span>";
		}
	}
	else
	{
		echo "Error selecting database: <span class=\"red\">".mysql_error().".</span>";
	}

mysql_close($connect);
?>
</body>
</html>