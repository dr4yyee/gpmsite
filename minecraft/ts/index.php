<!DOCTYPE html>
<html>
<head>
    <title>&gt; GPM-Server  &lt;</title>

    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <link href="style.css" type="text/css" rel="stylesheet">
</head>

<body>
    <div align="center" id="text">
        <div align="justify" id="section">
            <h1 style="color: white;">TeamSpeak Viewer</h1>
            <div align="center">
                <iframe frameborder="0" id="viewer" src="/webviewer" width="95%" height="600" name="TS3Viewer" marginheight="10" marginwidth="10">
                    <p>Ihr Browser kann leider keine eingebetteten Frames anzeigen:
                    Sie k&ouml;nnen die eingebettete Seite &uuml;ber den folgenden Verweis
                    aufrufen: 
                    <a href="/webviewer">TS-Viewer</a></p>
                </iframe>
            </div>
        </div>
    </div>
    <div align="center">
        <div id="links" align="justify">
            <a href="ts3server://gpm-server.de?server_uid=DYNzDxxaxO7udr4ujBZhwybMeYk%3D"><div>Server betreten</div></a>
            <a href="ts3server://gpm-server.de?port=9987&addbookmark=GPM%20Official%20TS3%20Server"><div>Lesezeichen speichern</div></a>
            <a href="http://gpm-server.de/"><div style="margin-top: 10px" align="center">Zur&uuml;ck</div></a>
        </div>
    </div>
</body>
</html>