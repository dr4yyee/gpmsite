<!DOCTYPE html>
<html>

<head>
    <title>&gt; GPM-Server  &lt;</title>

    <meta charset="UTF-8">
	<meta name="author" content="PlaySteph310">
	<meta name="copyright" content="PlaySteph310">
	<meta name="description" content="GPM-Server reloaded! Trete unserer Community bei und erlebe neuen Spielspaß wie DU ihn nicht kennst!">
	<meta name="keywords" content="blox, minecraft, server, 1.7.7, neu, bukkit, spigot, plugins, lagfrei, lustig, freebuild, games, events, gs, grundstück, überleben, survival, kreativ, creative, vote, nocheatplus, lwc, cool, freistadt, gpm-server, gpm, join, now, build, pvp, games, lobby, immer online, selbstgeschrieben plugins, erfahrenes team, 1.7.10, 1.8, komlett, skyblock, creative">

<?php
function check_mobile() {
  $agents = array(
    'Windows CE', 'Pocket', 'Mobile',
    'Portable', 'Smartphone', 'SDA',
    'PDA', 'Handheld', 'Symbian',
    'WAP', 'Palm', 'Avantgo',
    'cHTML', 'BlackBerry', 'Opera Mini',
    'Nokia', 'Android'
  );

  // Pr�fen der Browserkennung
  for ($i=0; $i<count($agents); $i++) {
    if(isset($_SERVER["HTTP_USER_AGENT"]) && strpos($_SERVER["HTTP_USER_AGENT"], $agents[$i]) !== false)
      return true;
  }

  return false;
}

$style = '';

if(check_mobile())
{
    $style = 'handheld.css';
    ?>
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <?php
}
else
{
    $style = 'style.css';
}
 ?>
     <link href="<?php echo $style ?>" type="text/css" rel="stylesheet">

</head>
<body>
    <div align="center" id="text">
            <h1>Bewerbungsinfos</h1>
        <div align="justify" id="section">
        <h3>Wie und wo bewirbt man sich?</h3>
        <p>Wenn ihr euch bewerben wollt, dann m&uuml;sst ihr euch unter<br> <a href="http://forum.gpm-server.de">forum.gpm-server.de</a> registrieren. Dann geht ihr auf die Kategorie Team-Infos und dort auf Bewerbung. Dort klickt ihr auf Thema erstellen und schreibt eure Bewerbung.</p>
        <h3>Was in deiner Bewerbung stehen sollte</h3>
        <u><b>1. Pers&ouml;nliche Daten</b></u>
        <ul>
            <li>Vorname (Nachname optional)</li>
            <li>Alter</li>
            <li>In-Game Name</li>
            <li>(Sonstige Daten optional)</li>
        </ul>
        <u><b>2. Selbsteinsch&auml;tzung / Referenzen</b></u>
        <ul>
            <li>Nenne jeweils etwa 3 Positive und Negative Eigenschaften von dir</li>
            <li>Was sind deine Referenzen? Warst du schonmal als Admin, Supporter, Moderator, etc. t&auml;tig?</li>
            <li>Was macht dich zu einem geeigneten Teammitglied?</li>
            <li>Was hast du vor auf dem Server zu tun oder zu ver&auml;ndern?</li>
            <li>Seit wann (welcher Version) spielst du Minecraft?</li>
            <li>Wie ist dein derzeitiger Bildungsgrad? Hast du Probleme in der Schule? (optional)</li>
            <li>Kannst du Ruhe bewahren? Mit Usern gibt es Oft Auseinandersetzung bei denen du ruhig reagieren solltest</li>
        </ul>
        <u><b>2.1 Servererfahrungen</b></u>
        <ul>
            <li>Wenn m&ouml;glich eine Liste erstellen mit Plugins, mit denen Du Erfahrung hast</li>
            <li>Gute Computerhandhabung und deine Computerst&auml;rken kannst du hier auch reinschreiben</li>
            <li>(zu Erfahrung z&auml;hlt nicht wenn man z.B. den //set Befehl von WorldEdit kennt, sondern wirkliche Erfahrung in Konfiguration und Handhabung)</li>
        </ul>
        <u><b>3. Layout</b></u>
        <ul>
            <li>Die Bewerbung <b>NICHT</b> komplett in Stichpunkten schreiben, wir möchten einen Flie&szlig;text sehen</li>
            <li>Keine Emoticons (Smileys) verwenden</li>
            <li>Schriftart sollte eine Standartm&auml;ßige Schriftart (Arial, Verdana, Tahoma) sein in 10 bis 12 pt</li>
            <li>Gern gesehen sind Links zu Bildern von früheren Projekten (Diese darf keine fremde Serverwerbung enthalten)</li>
        </ul>
        <u><b>4. Kontakt</b></u>
        <ul>
            <li>TeamSpeak³ und/oder Skype sowie ein funktionierendes Mikrofon (Kein Rauschen, etc.) sind Pflicht</li>
            <li>Weitere Kontaktm&ouml;glichkeiten sind empfohlen</li>
        </ul>
        <u><b>5. Ehrliche Angaben</b></u>
        <p>Falsche Angaben in einer Bewerbung werden als Betrug gesehen und dem entsprechend behandelt. (Schlimme F&auml;lle können zu einem Ban führen!) Als Bestätigung verlangen wir, dass du den folgenden Satz unter deine Bewerbung schreibst:

        <br><b>Ich versichere, dass diese Bewerbung von mir geschrieben wurde und sie komplett der Wahrheit entspricht.</b></p>
        </div>
    </div>
    <div align="center">
        <div id="links" align="justify">
            <a href="javascript:history.back();"><div align="center">Zur&uuml;ck</div></a>
        </div>
    </div>
</body>
</html>