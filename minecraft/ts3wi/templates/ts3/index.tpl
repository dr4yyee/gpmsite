﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
*Copyright (C) 2010-2011  Psychokiller
*
*This program is free software; you can redistribute it and/or modify it under the terms of 
*the GNU General Public License as published by the Free Software Foundation; either 
*version 3 of the License, or any later version.
*
*This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
*without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
*See the GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License along with this program; if not, see http://www.gnu.org/licenses/. 
-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">
<head>
<title>Teamspeak 3 - Webinterface</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="templates/{$tmpl}/gfx/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="templates/{$tmpl}/gfx/tsview.css" type="text/css" media="screen" />
<script type="text/javascript">
//<![CDATA[
function Klappen(Id) 
	{
	
	if(Id == 0)
		{
		var i = 1;
		var jetec_Minus="gfx/images/minus.png", jetec_Plus="gfx/images/plus.png";
		
		if(document.getElementById('Pic0').name == 'plus')
			{
			document.getElementById('Pic0').src = jetec_Minus;
			document.getElementById('Pic0').name = 'minus';
			var openAll = 1;
			}
			else
			{
			document.getElementById('Pic0').src = jetec_Plus;
			document.getElementById('Pic0').name = 'plus';
			var openAll = 0;
			}
		while(i<100)
			{
			if(document.getElementById('Pic'+i)!=null)
				{
				var KlappText = document.getElementById('Lay'+i);
				var KlappBild = document.getElementById('Pic'+i);
				if (openAll == 1) 
					{
					KlappText.style.display = 'block';
					KlappBild.src = jetec_Minus;
					} 
					else 
					{
					KlappText.style.display = 'none';
					KlappBild.src = jetec_Plus;
					}
				i++;
				}
				else
				{
				break;
				}
			}
		}
	var KlappText = document.getElementById('Lay'+Id);
	var KlappBild = document.getElementById('Pic'+Id);
	var jetec_Minus="gfx/images/minus.png", jetec_Plus="gfx/images/plus.png";
	if (KlappText.style.display == 'none') 
		{
		KlappText.style.display = 'block';
		KlappBild.src = jetec_Minus;
		} 
		else 
		{
		KlappText.style.display = 'none';
		KlappBild.src = jetec_Plus;
		}
	}

function oeffnefenster (url) {
 fenster = window.open(url, "fenster1", "width=350,height=150,status=no,scrollbars=yes,resizable=no");
 fenster.opener.name="opener";
 fenster.focus();
}

function hide_select(selectId)
	{
	if(selectId==0)
		{
		document.getElementById("groups").style.display = "";
		document.getElementById("servergroups").style.display = "";
		document.getElementById("channelgroups").style.display = "none";
		document.getElementById("channel").style.display = "none";
		}
	  else if (selectId==1)
		{
		document.getElementById("groups").style.display = "";
		document.getElementById("servergroups").style.display = "none";
		document.getElementById("channelgroups").style.display = "";
		document.getElementById("channel").style.display = "";
		}
		else
		{
		document.getElementById("groups").style.display = "none";
		document.getElementById("servergroups").style.display = "none";
		document.getElementById("channelgroups").style.display = "none";
		document.getElementById("channel").style.display = "none";
		}
	}

var checkflag = "false";

function check(form) 
	{
	if (checkflag == "false") 
		{
		for (i = 0; i < document.forms[form].elements.length; i++) 
			{
			if(document.forms[form].elements[i].name != 'checkall')
				{
				document.forms[form].elements[i].checked = true;
				}
			}
		checkflag = "true";
		return checkflag; 
		}
		else 
		{
		for (i = 0; i < document.forms[form].elements.length; i++) 
			{
				document.forms[form].elements[i].checked = false;
			}
		checkflag = "false";
		return checkflag; 
		}
	}
var conf_arr = new Array();
function confirmArray(sid, name, port, action)
	{
	conf_arr[sid]=new Object();
	conf_arr[sid]['name']=name;
	conf_arr[sid]['port']=port;
	if(document.getElementById("caction"+sid).options.selectedIndex == 0)
		{
		conf_arr[sid]['action']='';
		}
		else if(document.getElementById("caction"+sid).options.selectedIndex == 1)
		{
		conf_arr[sid]['action']='start';
		}
		else if(document.getElementById("caction"+sid).options.selectedIndex == 2)
		{
		conf_arr[sid]['action']='stop';
		}
		else if(document.getElementById("caction"+sid).options.selectedIndex == 3)
		{
		conf_arr[sid]['action']='del';
		}
	}
	
function confirmAction()
	{
	var text="Möchtest du folgende Aktion wirklich ausführen?\n\n";
	for(var i in conf_arr)
		{
		if(conf_arr[i]['action'] == 'start')
			{
			text = text+"***Starten*** "+conf_arr[i]['name']+" "+conf_arr[i]['port']+"\n";
			}
			else if(conf_arr[i]['action'] == 'stop')
			{
			text = text+"***Stoppen*** "+conf_arr[i]['name']+" "+conf_arr[i]['port']+"\n";
			}
			else if(conf_arr[i]['action'] == 'del')
			{
			text = text+"***Löschen*** "+conf_arr[i]['name']+" "+conf_arr[i]['port']+"\n";
			}
		}
	return text;
	}
//]]>
</script>
</head>
<body>
<table align="center" class="border" style="width:1000px; background-color:#FFFFFF;" cellpadding="1" cellspacing="0">
	<tr>
	  <td colspan="2"><table id="Tabelle_01" width="1000" height="314" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td rowspan="2">
			<img src="templates/{$tmpl}/gfx/images/header_01.jpg" width="434" height="104" alt=""></td>
		<td colspan="3">
			<img src="templates/{$tmpl}/gfx/images/header_02.jpg" width="566" height="70" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="templates/{$tmpl}/gfx/images/header_03.jpg" width="566" height="34" alt=""></td>
	</tr>
	<tr>
		<td colspan="4">
			<img src="templates/{$tmpl}/gfx/images/header_04.jpg" width="1000" height="51" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" rowspan="3">
			<img src="templates/{$tmpl}/gfx/images/header_05.jpg" width="675" height="158" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/header_06.jpg" width="291" height="15" alt=""></td>
		<td rowspan="3">
			<img src="templates/{$tmpl}/gfx/images/header_07.jpg" width="34" height="158" alt=""></td>
	</tr>
	<tr>
		<td><a href="http://www.teamspeak.com/?page=downloads&type=ts3_windows_client_latest" target="_blank"><img src="templates/{$tmpl}/gfx/images/header_08.jpg" alt="" width="291" height="75" border="0"></a></td>
	</tr>
	<tr>
		<td>
			<img src="templates/{$tmpl}/gfx/images/header_09.jpg" width="291" height="68" alt=""></td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="434" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="241" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="291" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="34" height="1" alt=""></td>
	</tr>
</table></td>
  </tr>
	<tr>
		<td colspan="2">{include file="{$tmpl}/head.tpl"}</td>
	</tr>
	{include file="{$tmpl}/showupdate.tpl"}
	<tr valign="top">	
		{include file="{$tmpl}/mainbar.tpl"}
		<td align="center">
		{include file="{$tmpl}/{$site}.tpl"}
		</td>
	</tr>
	<tr valign="top">
	  <td height="35px" align="center">&nbsp;</td>
  </tr>
	<tr>
		<td colspan="2"><table border="0" width="1001" height="334" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4" rowspan="3">
			<img src="templates/{$tmpl}/gfx/images/footer_01.jpg" width="153" height="78" alt=""></td>
		<td colspan="8">
			<img src="templates/{$tmpl}/gfx/images/footer_02.jpg" width="244" height="32" alt=""></td>
		<td colspan="2" rowspan="3">
			<img src="templates/{$tmpl}/gfx/images/footer_03.jpg" width="604" height="78" alt=""></td>
	</tr>
	<tr>
		<td><a href="http://www.twitter.com/teamspeak" target="_blank"><img src="templates/{$tmpl}/gfx/images/footer_04.jpg" alt="" width="38" height="41" border="0"></a></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/footer_05.jpg" width="12" height="41" alt=""></td>
		<td colspan="2"><a href="http://www.facebook.com/teamspeak" target="_blank"><img src="templates/{$tmpl}/gfx/images/footer_06.jpg" alt="" width="37" height="41" border="0"></a></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/footer_07.jpg" width="14" height="41" alt=""></td>
		<td><a href="http://www.teamspeak.com/rss.php?type=news&lang=en" target="_blank"><img src="templates/{$tmpl}/gfx/images/footer_08.jpg" alt="" width="36" height="41" border="0"></a></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/footer_09.jpg" width="10" height="41" alt=""></td>
		<td><a href="http://www.youtube.com/teamspeak" target="_blank"><img src="templates/{$tmpl}/gfx/images/footer_10.jpg" alt="" width="97" height="41" border="0"></a></td>
	</tr>
	<tr>
		<td colspan="8">
			<img src="templates/{$tmpl}/gfx/images/footer_11.jpg" width="244" height="5" alt=""></td>
	</tr>
	<tr>
		<td colspan="7">
			<img src="templates/{$tmpl}/gfx/images/footer_12.jpg" width="223" height="30" alt=""></td>
		<td colspan="6" rowspan="8">
			<img src="templates/{$tmpl}/gfx/images/footer_13.jpg" width="509" height="179" alt=""></td>
		<td rowspan="8">
			<img src="templates/{$tmpl}/gfx/images/footer_14.jpg" width="269" height="179" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="templates/{$tmpl}/gfx/images/footer_15.jpg" width="14" height="108" alt=""></td>
		<td width="98"  height="18" background="templates/{$tmpl}/gfx/images/footer_16.jpg";><A href="http://www.teamspeak.com/?page=downloads" target="_blank">TeamSpeak 3</A></td>
		<td rowspan="6">
			<img src="templates/{$tmpl}/gfx/images/footer_17.jpg" width="14" height="108" alt=""></td>
		<td colspan="4"width="98"  height="18" background="templates/{$tmpl}/gfx/images/footer_18.jpg";><A href="http://www.teamspeak.com/?page=faq" target="_blank">FAQ</A></td>
	</tr>
	<tr>
		<td width="98"  height="18" background="templates/{$tmpl}/gfx/images/footer_19.jpg";><A href="http://www.teamspeak.com/?page=downloads&archive=1" target="_blank">TeamSpeak 2</A></td>
		<td colspan="4"width="98"  height="18" background="templates/{$tmpl}/gfx/images/footer_20.jpg";><A href="http://www.teamspeak.com/?page=tutorials" target="_blank">Video Tutorials</A></td>
	</tr>
	<tr>
		<td width="98"  height="18" background="templates/{$tmpl}/gfx/images/footer_21.jpg";><A href="http://addons.teamspeak.com/" target="_blank">Addons</A></td>
		<td colspan="4"width="98"  height="18" background="templates/{$tmpl}/gfx/images/footer_22.jpg";><A href="http://www.teamspeak.com/?page=getstarted" target="_blank">Getting Started</A></td>
	</tr>
	<tr>
		<td width="98"  height="18" background="templates/{$tmpl}/gfx/images/footer_24.jpg";><A href="http://www.teamspeak.com/?page=mediakit" target="_blank">Media Kit</A></td>
		<td colspan="4"width="98"  height="18" background="templates/{$tmpl}/gfx/images/footer_25.jpg";><A href="http://www.teamspeak.com/?page=premiumsupport" target="_blank">Premium Support</A></td>
	</tr>
	<tr>
		<td width="98"  height="18" background="templates/{$tmpl}/gfx/images/footer_27.jpg";><a href="http://ts3.cs-united.de/forum/viewforum.php?f=2" target="_blank">PK Webinterface</a></td>
		<td colspan="4"width="98"  height="18" background="templates/{$tmpl}/gfx/images/footer_28.jpg";><a href="http://ts3.cs-united.de/forum/portal.php" target="_blank">PK Support</a></td>
	</tr>
	<tr>
		<td width="98"  height="18" background="templates/{$tmpl}/gfx/images/footer_30.jpg";>&nbsp;</td>
		<td colspan="4"width="98"  height="18" background="templates/{$tmpl}/gfx/images/footer_29.jpg";></td>
	</tr>
	<tr>
		<td colspan="7">
			<img src="templates/{$tmpl}/gfx/images/footer_31.jpg" width="223" height="41" alt=""></td>
	</tr>
	<tr>
		<td colspan="14" align="center" width="1001"  height="105" background="templates/{$tmpl}/gfx/images/footer_32.jpg" valign="top";>{$footer}<br />
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
		<input type="hidden" name="cmd" value="_s-xclick" />
		<input type="hidden" name="hosted_button_id" value="DHMCG2WNSE62J" />
		<input class="sbutton" type="image" src="https://www.paypal.com/de_DE/DE/i/btn/btn_donate_SM.gif" name="submit" alt="Jetzt einfach, schnell und sicher online bezahlen – mit PayPal." />
		<img alt="" border="0" src="https://www.paypal.com/de_DE/i/scr/pixel.gif" width="1" height="1" />
		</form>
<br />
This TeamSpeak 3 Web Interface Design is created by Blade @ <a href="http://www.sw-og.de" target="_blank">Shadow World Of Games</a><br />
		All images are protected by copyright law and are registered by Copyright &copy; 2011 <a href="http://www.teamspeak.com" target="_blank">TeamSpeak Systems GmbH</a>. All rights reserved. </td>
	</tr>
	<tr>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="14" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="98" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="14" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="27" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="38" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="12" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="20" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="17" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="14" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="36" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="10" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="97" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="335" height="1" alt=""></td>
		<td>
			<img src="templates/{$tmpl}/gfx/images/Abstandhalter.gif" width="269" height="1" alt=""></td>
	</tr>
</table></td>
	</tr>
</table>
<script language="JavaScript" type="text/javascript" src="gfx/js/wz_tooltip.js"></script>
</body>
</html>