<?php /* Smarty version Smarty3rc4, created on 2014-08-14 18:58:35
         compiled from "/var/www/httpdocs/ts3wi/templates/bf3/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:144168563653eceabb36e245-15937544%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '28c608017fe1439a60fdfbd126b7a5f4e7a45c95' => 
    array (
      0 => '/var/www/httpdocs/ts3wi/templates/bf3/index.tpl',
      1 => 1408033904,
    ),
  ),
  'nocache_hash' => '144168563653eceabb36e245-15937544',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
*Copyright (C) 2010-2011  Psychokiller
*
*This program is free software; you can redistribute it and/or modify it under the terms of 
*the GNU General Public License as published by the Free Software Foundation; either 
*version 3 of the License, or any later version.
*
*This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
*without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
*See the GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License along with this program; if not, see http://www.gnu.org/licenses/. 
-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="de">
<head>
<title>Teamspeak 3 - Webinterface</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/tsview.css" type="text/css" media="screen" />
<script type="text/javascript">
//<![CDATA[
function Klappen(Id) 
	{
	
	if(Id == 0)
		{
		var i = 1;
		var jetec_Minus="gfx/images/minus.png", jetec_Plus="gfx/images/plus.png";
		
		if(document.getElementById('Pic0').name == 'plus')
			{
			document.getElementById('Pic0').src = jetec_Minus;
			document.getElementById('Pic0').name = 'minus';
			var openAll = 1;
			}
			else
			{
			document.getElementById('Pic0').src = jetec_Plus;
			document.getElementById('Pic0').name = 'plus';
			var openAll = 0;
			}
		while(i<100)
			{
			if(document.getElementById('Pic'+i)!=null)
				{
				var KlappText = document.getElementById('Lay'+i);
				var KlappBild = document.getElementById('Pic'+i);
				if (openAll == 1) 
					{
					KlappText.style.display = 'block';
					KlappBild.src = jetec_Minus;
					} 
					else 
					{
					KlappText.style.display = 'none';
					KlappBild.src = jetec_Plus;
					}
				i++;
				}
				else
				{
				break;
				}
			}
		}
	var KlappText = document.getElementById('Lay'+Id);
	var KlappBild = document.getElementById('Pic'+Id);
	var jetec_Minus="gfx/images/minus.png", jetec_Plus="gfx/images/plus.png";
	if (KlappText.style.display == 'none') 
		{
		KlappText.style.display = 'block';
		KlappBild.src = jetec_Minus;
		} 
		else 
		{
		KlappText.style.display = 'none';
		KlappBild.src = jetec_Plus;
		}
	}

function oeffnefenster (url) {
 fenster = window.open(url, "fenster1", "width=350,height=150,status=no,scrollbars=yes,resizable=no");
 fenster.opener.name="opener";
 fenster.focus();
}

function hide_select(selectId)
	{
	if(selectId==0)
		{
		document.getElementById("groups").style.display = "";
		document.getElementById("servergroups").style.display = "";
		document.getElementById("channelgroups").style.display = "none";
		document.getElementById("channel").style.display = "none";
		}
	  else if (selectId==1)
		{
		document.getElementById("groups").style.display = "";
		document.getElementById("servergroups").style.display = "none";
		document.getElementById("channelgroups").style.display = "";
		document.getElementById("channel").style.display = "";
		}
		else
		{
		document.getElementById("groups").style.display = "none";
		document.getElementById("servergroups").style.display = "none";
		document.getElementById("channelgroups").style.display = "none";
		document.getElementById("channel").style.display = "none";
		}
	}

var checkflag = "false";

function check(form) 
	{
	if (checkflag == "false") 
		{
		for (i = 0; i < document.forms[form].elements.length; i++) 
			{
			if(document.forms[form].elements[i].name != 'checkall')
				{
				document.forms[form].elements[i].checked = true;
				}
			}
		checkflag = "true";
		return checkflag; 
		}
		else 
		{
		for (i = 0; i < document.forms[form].elements.length; i++) 
			{
				document.forms[form].elements[i].checked = false;
			}
		checkflag = "false";
		return checkflag; 
		}
	}
var conf_arr = new Array();
function confirmArray(sid, name, port, action)
	{
	conf_arr[sid]=new Object();
	conf_arr[sid]['name']=name;
	conf_arr[sid]['port']=port;
	if(document.getElementById("caction"+sid).options.selectedIndex == 0)
		{
		conf_arr[sid]['action']='';
		}
		else if(document.getElementById("caction"+sid).options.selectedIndex == 1)
		{
		conf_arr[sid]['action']='start';
		}
		else if(document.getElementById("caction"+sid).options.selectedIndex == 2)
		{
		conf_arr[sid]['action']='stop';
		}
		else if(document.getElementById("caction"+sid).options.selectedIndex == 3)
		{
		conf_arr[sid]['action']='del';
		}
	}
	
function confirmAction()
	{
	var text="Möchtest du folgende Aktion wirklich ausführen?\n\n";
	for(var i in conf_arr)
		{
		if(conf_arr[i]['action'] == 'start')
			{
			text = text+"***Starten*** "+conf_arr[i]['name']+" "+conf_arr[i]['port']+"\n";
			}
			else if(conf_arr[i]['action'] == 'stop')
			{
			text = text+"***Stoppen*** "+conf_arr[i]['name']+" "+conf_arr[i]['port']+"\n";
			}
			else if(conf_arr[i]['action'] == 'del')
			{
			text = text+"***Löschen*** "+conf_arr[i]['name']+" "+conf_arr[i]['port']+"\n";
			}
		}
	return text;
	}
//]]>
</script>
</head>
<body>
<table align="center" class="border" style="width:1000px; background-color:#ffffff;" cellpadding="1" cellspacing="0">
	<tr>
	  <td colspan="2"></td>
  </tr>
	<tr>
		<td colspan="2"><?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tmpl')->value)."/head.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?></td>
	</tr>
	<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tmpl')->value)."/showupdate.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	<tr valign="top">
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tmpl')->value)."/mainbar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
		<td align="center">
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tmpl')->value)."/".($_smarty_tpl->getVariable('site')->value).".tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
		</td>
	</tr>
	<tr valign="top">
	  <td align="center">&nbsp;</td>
  </tr>
	<tr>
		<td colspan="2" class="footer">
		<table id="Tabelle_01" width="1000" height="377" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="10">
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_01.jpg" width="1000" height="49" alt=""></td>
	</tr>
	<tr>
		<td rowspan="11">
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_02.jpg" width="21" height="157" alt=""></td>
		<td width="215" height="23" colspan="2" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_03.jpg"><strong><span style="color:#f88c14">Battlefield 3: BF3</span></strong></td>
		<td rowspan="11">
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_04.jpg" width="29" height="157" alt=""></td>
		<td width="214" height="23" colspan="2" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_05.jpg"><strong><span style="color:#f88c14">Support</span></strong></td>
		<td rowspan="11">
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_06.jpg" width="29" height="157" alt=""></td>
		<td width="215" height="23" colspan="2" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_07.jpg"><strong><span style="color:#f88c14">Feature</span></strong></td>
		<td rowspan="11">
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_08.jpg" width="277" height="157" alt=""></td>
	</tr>
	<tr>
		<td rowspan="10">
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_09.jpg" width="30" height="134" alt=""></td>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_10.jpg" width="185" height="4" alt=""></td>
		<td rowspan="10">
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_11.jpg" width="30" height="134" alt=""></td>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_12.jpg" width="184" height="4" alt=""></td>
		<td rowspan="10">
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_13.jpg" width="30" height="134" alt=""></td>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_14.jpg" width="185" height="4" alt=""></td>
	</tr>
	<tr>
		<td width="185" height="23" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_15.jpg"><a href="http://forum.ea.com/de/categories/show/68.page" target="_blank">BF3: X-Box 360</a></td>
		<td width="184" height="23" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_16.jpg"><A href="http://www.teamspeak.com/?page=faq" target="_blank">FAQ</A></td>
		<td width="185" height="23" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_17.jpg"><A href="http://www.teamspeak.com/?page=downloads" target="_blank">TeamSpeak 3</A></td>
	</tr>
	<tr>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_18.jpg" width="185" height="4" alt=""></td>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_19.jpg" width="184" height="4" alt=""></td>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_20.jpg" width="185" height="4" alt=""></td>
	</tr>
	<tr>
		<td width="185" height="23" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_21.jpg"><a href="http://forum.ea.com/de/categories/show/68.page" target="_blank">BF3: Playstation 3</a></td>
		<td width="184" height="23" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_22.jpg"><A href="http://www.teamspeak.com/?page=tutorials" target="_blank">Video Tutorials</A></td>
		<td width="185" height="23" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_23.jpg"><A href="http://addons.teamspeak.com/" target="_blank">Addons</A></td>
	</tr>
	<tr>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_24.jpg" width="185" height="4" alt=""></td>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_25.jpg" width="184" height="4" alt=""></td>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_26.jpg" width="185" height="4" alt=""></td>
	</tr>
	<tr>
		<td width="185" height="23" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_27.jpg"><a href="http://forum.ea.com/de/categories/show/68.page" target="_blank">BF3:  PC</a></td>
		<td width="184" height="23" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_28.jpg"><A href="http://www.teamspeak.com/?page=getstarted" target="_blank">Getting Started</A></td>
		<td width="185" height="23" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_29.jpg"><A href="http://www.teamspeak.com/?page=mediakit" target="_blank">Media Kit</A></td>
	</tr>
	<tr>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_30.jpg" width="185" height="4" alt=""></td>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_31.jpg" width="184" height="4" alt=""></td>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_32.jpg" width="185" height="4" alt=""></td>
	</tr>
	<tr>
		<td width="185" height="23" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_33.jpg"><a href="http://forum.ea.com/de/categories/show/68.page" target="_blank">BF3: Wii</a></td>
		<td width="184" height="23" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_34.jpg"><a href="http://ts3.cs-united.de/forum/portal.php" target="_blank">PK Support</a></td>
		<td width="185" height="23" align="left" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_35.jpg"><a href="http://ts3.cs-united.de/forum/viewforum.php?f=2" target="_blank">PK Webinterface</a></td>
	</tr>
	<tr>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_36.jpg" width="185" height="4" alt=""></td>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_37.jpg" width="184" height="4" alt=""></td>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_38.jpg" width="185" height="4" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_39.jpg" width="185" height="22" alt=""></td>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_40.jpg" width="184" height="22" alt=""></td>
		<td>
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_41.jpg" width="185" height="22" alt=""></td>
	</tr>
	<tr>
		<td colspan="10">
			<img src="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_42.jpg" width="1000" height="33" alt=""></td>
	</tr>
	<tr>
		<td colspan="10" align="center" width="1000" height="138" valign="top" background="templates/<?php echo $_smarty_tpl->getVariable('tmpl')->value;?>
/gfx/images/footer_43.jpg"><span style="color:#f88c14"><?php echo $_smarty_tpl->getVariable('footer')->value;?>
<br />
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
		<input type="hidden" name="cmd" value="_s-xclick" />
		<input type="hidden" name="hosted_button_id" value="DHMCG2WNSE62J" />
		<input class="sbutton" type="image" src="https://www.paypal.com/de_DE/DE/i/btn/btn_donate_SM.gif" name="submit" alt="Jetzt einfach, schnell und sicher online bezahlen – mit PayPal." />
		<img alt="" border="0" src="https://www.paypal.com/de_DE/i/scr/pixel.gif" width="1" height="1" />
		</form></span>
        <p><span style="color:#f88c14">This TeamSpeak 3 Web Interface Design is created by Blade @ <a href="http://www.sw-og.de" target="_blank">Shadow World Of Games</a><br />
          All images are protected by copyright law and are registered by Copyright © 2011 Electronic Arts GmbH<BR />All rights reserved.</span></p><br /></td>
	</tr>
</table>
		</td>
	</tr>
</table>
<script language="JavaScript" type="text/javascript" src="gfx/js/wz_tooltip.js"></script>
</body>
</html>