<?php /* Smarty version Smarty3rc4, created on 2014-08-14 18:40:10
         compiled from "/var/www/httpdocs/ts3wi/templates/ts3/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:81869079353ece66abcc8a3-52408408%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd9b49405daf4854e463cd287d2674c1e5f1be74f' => 
    array (
      0 => '/var/www/httpdocs/ts3wi/templates/ts3/login.tpl',
      1 => 1408034050,
    ),
  ),
  'nocache_hash' => '81869079353ece66abcc8a3-52408408',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!empty($_smarty_tpl->getVariable('error')->value)){?>
<table>
	<tr>
		<td class="error"><?php echo $_smarty_tpl->getVariable('error')->value;?>
</td>
	</tr>
</table>
<?php }?>
<?php if (!empty($_smarty_tpl->getVariable('motd')->value)){?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td class="logintop login"></td>
	</tr>
	<tr>
		<td class="loginpuff loginhead"><?php echo $_smarty_tpl->getVariable('lang')->value['motd'];?>
</td>
	</tr>
	<tr>
		<td class="loginpuff" align="center">
		<?php echo $_smarty_tpl->getVariable('motd')->value;?>

		</td>
	</tr>
	<tr>
		<td class="loginbottom1">&nbsp;</td>
	</tr>
</table>
<?php }?>
<?php if (!isset($_POST['sendlogin'])&&$_smarty_tpl->getVariable('loginstatus')->value!==true||$_smarty_tpl->getVariable('loginstatus')->value!==true){?>
<form method="post" action="index.php?site=login">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td class="logintop" colspan="2"></td>
	</tr>
	<tr>
		<td class="loginpuff loginhead" colspan="2"><?php echo $_smarty_tpl->getVariable('lang')->value['login'];?>
</td>
	</tr>
	<tr>
		<td class="loginpuff" align="center">
		<table style="padding:10px;" cellpadding="1" cellspacing="0">
			<tr>
				<td class="login"><?php echo $_smarty_tpl->getVariable('lang')->value['server'];?>
:</td>
				<td>
				<?php if (count($_smarty_tpl->getVariable('instances')->value)==1){?>
					<?php  $_smarty_tpl->tpl_vars['sdata'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['skey'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('instances')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['sdata']->key => $_smarty_tpl->tpl_vars['sdata']->value){
 $_smarty_tpl->tpl_vars['skey']->value = $_smarty_tpl->tpl_vars['sdata']->key;
?>
					<input type="hidden" name="skey" value="<?php echo $_smarty_tpl->tpl_vars['skey']->value;?>
" />	<?php echo $_smarty_tpl->tpl_vars['sdata']->value['alias'];?>
 
					<?php }} ?>
				<?php }else{ ?>
				<select name="skey">
				<?php  $_smarty_tpl->tpl_vars['sdata'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['skey'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('instances')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['sdata']->key => $_smarty_tpl->tpl_vars['sdata']->value){
 $_smarty_tpl->tpl_vars['skey']->value = $_smarty_tpl->tpl_vars['sdata']->key;
?>
				<option value="<?php echo $_smarty_tpl->tpl_vars['skey']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['sdata']->value['alias'];?>
</option>	
				<?php }} ?>
				</select>
				<?php }?>
				</td>
			</tr>
			<tr>
				<td class="login"><?php echo $_smarty_tpl->getVariable('lang')->value['username'];?>
:</td>
				<td class="login"><input type="text" name="loginUser" value="serveradmin" /></td>
			</tr>
			<tr>
				<td class="login"><?php echo $_smarty_tpl->getVariable('lang')->value['password'];?>
:</td>
				<td class="login"><input type="password" name="loginPw" /></td>
			</tr>
			<?php if ($_smarty_tpl->getVariable('serverhost')->value===true){?>
				<tr>
					<td class="login"><?php echo $_smarty_tpl->getVariable('lang')->value['port'];?>
:</td>
					<td class="login"><input type="text" name="loginPort" value="" /></td>
				</tr>
			<?php }?>
			<tr>
				<td class="login"><?php echo $_smarty_tpl->getVariable('lang')->value['option'];?>
:</td>
				<td><input class="button" type="submit" name="sendlogin" value="<?php echo $_smarty_tpl->getVariable('lang')->value['login'];?>
"/></td>
			</tr>
			<?php if ($_smarty_tpl->getVariable('serverhost')->value===true){?>
			<tr>
				<td colspan="2" style="text-align:center"><a href="index.php?site=hostlogin"><?php echo $_smarty_tpl->getVariable('lang')->value['hostlogin'];?>
</a></td>
			</tr>
			<?php }?>
		</table>
		
		</td>
	</tr>
	<tr>
		<td class="loginbottom2">&nbsp;</td>
	</tr>
</table>
</form>
<?php }?>