<?php /* Smarty version Smarty3rc4, created on 2014-08-14 18:42:26
         compiled from "/var/www/httpdocs/ts3wi/templates/ts3/logview.tpl" */ ?>
<?php /*%%SmartyHeaderCode:103371350653ece6f28952d8-84274355%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '35f6e94090b05d363f7cf3df748ad8ed4797ada1' => 
    array (
      0 => '/var/www/httpdocs/ts3wi/templates/ts3/logview.tpl',
      1 => 1408034050,
    ),
  ),
  'nocache_hash' => '103371350653ece6f28952d8-84274355',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/httpdocs/ts3wi/libs/Smarty/libs/plugins/modifier.date_format.php';
?><?php if (isset($_smarty_tpl->getVariable('permoverview')->value['b_virtualserver_log_view'])&&empty($_smarty_tpl->getVariable('permoverview')->value['b_virtualserver_log_view'])){?>
	<table class="border" style="width:100%;" cellpadding="1" cellspacing="0">
		<tr>
			<td class="thead"><?php echo $_smarty_tpl->getVariable('lang')->value['error'];?>
</td>
		</tr>
		<tr>
			<td class="green1"><?php echo $_smarty_tpl->getVariable('lang')->value['nopermissions'];?>
</td>
		</tr>
	</table>
<?php }else{ ?>
<form method="post" action="index.php?site=logview<?php if (!empty($_smarty_tpl->getVariable('port')->value)){?>&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
<?php }?>">
<table class="border" style="width:100%" cellspacing="0" cellpadding="0">
	<tr>
		<td class="thead" colspan="4"><?php echo $_smarty_tpl->getVariable('lang')->value['filter'];?>
</td>
	</tr>
	<tr>
		<td class="green1" colspan="4">
		<?php echo $_smarty_tpl->getVariable('lang')->value['limit'];?>
 (1-500):<input type="text" maxlength="3" size="1" name="limit" value="<?php if (isset($_POST['limit'])){?><?php echo $_POST['limit'];?>
<?php }?>" />
		</td>
	</tr>
	<tr>
		<td class="green1" colspan="4">
		<?php echo $_smarty_tpl->getVariable('lang')->value['error'];?>
:
		<input type="checkbox" <?php if (isset($_POST['type']['error'])&&$_POST['type']['error']==1){?>checked="checked"<?php }?> name="type[error]" value="1" />
		<?php echo $_smarty_tpl->getVariable('lang')->value['warning'];?>
:
		<input type="checkbox" <?php if (isset($_POST['type']['warning'])&&$_POST['type']['warning']==2){?>checked="checked"<?php }?> name="type[warning]" value="2" />
		<?php echo $_smarty_tpl->getVariable('lang')->value['debug'];?>
:
		<input type="checkbox" <?php if (isset($_POST['type']['debug'])&&$_POST['type']['debug']==3){?>checked="checked"<?php }?> name="type[debug]" value="3" />
		<?php echo $_smarty_tpl->getVariable('lang')->value['info'];?>
:
		<input type="checkbox" <?php if (isset($_POST['type']['info'])&&$_POST['type']['info']==4){?>checked="checked"<?php }?> name="type[info]" value="4" /><br />
		</td>
	</tr>
	<tr>
		<td class="green1" colspan="4">
		<?php echo $_smarty_tpl->getVariable('lang')->value['comparator'];?>
: <select name="comparator">
			<option value="" <?php if (isset($_POST['comparator'])&&empty($_POST['comparator'])){?>selected="selected"<?php }?>>-</option>
			<option value="<" <?php if (isset($_POST['comparator'])&&$_POST['comparator']=='<'){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getVariable('lang')->value['before'];?>
</option>
			<option value="=" <?php if (isset($_POST['comparator'])&&$_POST['comparator']=='='){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getVariable('lang')->value['exactly'];?>
</option>
			<option value=">" <?php if (isset($_POST['comparator'])&&$_POST['comparator']=='>'){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->getVariable('lang')->value['later'];?>
</option>
			</select>
		<?php echo $_smarty_tpl->getVariable('lang')->value['like'];?>
 
		<?php echo $_smarty_tpl->getVariable('lang')->value['date'];?>
 
			<select name="day">
			<option value="">-</option>
			<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['days']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['days']['name'] = 'days';
$_smarty_tpl->tpl_vars['smarty']->value['section']['days']['start'] = (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['days']['loop'] = is_array($_loop=32) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['days']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['days']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['days']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['days']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['days']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['days']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['days']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['days']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['days']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['days']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['days']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['days']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['days']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['days']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['days']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['days']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['days']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['days']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['days']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['days']['total']);
?>
				<option <?php if ($_POST['day']==$_smarty_tpl->getVariable('smarty')->value['section']['days']['index']){?>selected="selected"<?php }?> value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['days']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['days']['index'];?>
</option>
			<?php endfor; endif; ?>
			</select>
		    .<select name="month">
			<option value="">-</option>
			<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['months']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['months']['name'] = 'months';
$_smarty_tpl->tpl_vars['smarty']->value['section']['months']['start'] = (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['months']['loop'] = is_array($_loop=13) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['months']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['months']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['months']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['months']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['months']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['months']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['months']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['months']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['months']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['months']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['months']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['months']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['months']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['months']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['months']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['months']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['months']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['months']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['months']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['months']['total']);
?>
				<option <?php if ($_POST['month']==$_smarty_tpl->getVariable('smarty')->value['section']['months']['index']){?>selected="selected"<?php }?> value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['months']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['months']['index'];?>
</option>
			<?php endfor; endif; ?>
			</select>
		    .<select name="year">
			<option value="">-</option>
			<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['years']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['years']['name'] = 'years';
$_smarty_tpl->tpl_vars['smarty']->value['section']['years']['start'] = (int)1990;
$_smarty_tpl->tpl_vars['smarty']->value['section']['years']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('yearx')->value+1) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['years']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['years']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['years']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['years']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['years']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['years']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['years']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['years']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['years']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['years']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['years']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['years']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['years']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['years']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['years']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['years']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['years']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['years']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['years']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['years']['total']);
?>
				<option <?php if ($_POST['year']==$_smarty_tpl->getVariable('smarty')->value['section']['years']['index']){?>selected="selected"<?php }?> value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['years']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['years']['index'];?>
</option>
			<?php endfor; endif; ?>
			</select>
			- <?php echo $_smarty_tpl->getVariable('lang')->value['time'];?>

		    <select name="hour">
			<option value="">-</option>
			<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['hours']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['name'] = 'hours';
$_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['loop'] = is_array($_loop=25) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['hours']['total']);
?>
				<option <?php if ($_POST['hour']==$_smarty_tpl->getVariable('smarty')->value['section']['hours']['index']&&$_POST['hour']!=''){?>selected="selected"<?php }?> value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['hours']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['hours']['index'];?>
</option>
			<?php endfor; endif; ?>
			</select>
			:<select name="min">
			<option value="">-</option>
			<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['mins']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['name'] = 'mins';
$_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['loop'] = is_array($_loop=61) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['mins']['total']);
?>
				<option <?php if ($_POST['min']==$_smarty_tpl->getVariable('smarty')->value['section']['mins']['index']&&$_POST['min']!=''){?>selected="selected"<?php }?> value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['mins']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['mins']['index'];?>
</option>
			<?php endfor; endif; ?>
			</select>
			:<select name="sec">
			<option value="">-</option>
			<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['secs']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['name'] = 'secs';
$_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['loop'] = is_array($_loop=61) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['secs']['total']);
?>
				<option <?php if ($_POST['sec']==$_smarty_tpl->getVariable('smarty')->value['section']['secs']['index']&&$_POST['sec']!=''){?>selected="selected"<?php }?> value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['secs']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['secs']['index'];?>
</option>
			<?php endfor; endif; ?>
			</select>
			</td>
		</tr>
		<tr>
			<td colspan="4"> 
			<input type="submit" name="getfilter" value="<?php echo $_smarty_tpl->getVariable('lang')->value['search'];?>
" />
		</td>
	</tr>
</table>
</form>
<br />
<table class="border" style="width:100%" cellspacing="0" cellpadding="0">
	<tr>
		<td style="width:20%" class="thead"><?php echo $_smarty_tpl->getVariable('lang')->value['date'];?>
</td>
		<td style="width:5%" class="thead"><?php echo $_smarty_tpl->getVariable('lang')->value['level'];?>
</td>
		<td style="width:20%" class="thead"><?php echo $_smarty_tpl->getVariable('lang')->value['channel'];?>
</td>
		<td style="width:55%" class="thead"><?php echo $_smarty_tpl->getVariable('lang')->value['message'];?>
</td>
	</tr>

<?php if (!empty($_smarty_tpl->getVariable('serverlog')->value)){?>
	<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('serverlog')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
		<?php if (empty($_POST['type']['error'])&&empty($_POST['type']['warning'])&&empty($_POST['type']['debug'])&&empty($_POST['type']['info'])||$_POST['type']['error']==$_smarty_tpl->tpl_vars['value']->value['level']||$_POST['type']['warning']==$_smarty_tpl->tpl_vars['value']->value['level']||$_POST['type']['debug']==$_smarty_tpl->tpl_vars['value']->value['level']||$_POST['type']['info']==$_smarty_tpl->tpl_vars['value']->value['level']){?>
			<?php if ($_smarty_tpl->getVariable('change_col')->value%2){?> <?php $_smarty_tpl->tpl_vars['td_col'] = new Smarty_variable("green1", null, null);?> <?php }else{ ?> <?php $_smarty_tpl->tpl_vars['td_col'] = new Smarty_variable("green2", null, null);?> <?php }?>
			<tr>
				<td class="<?php echo $_smarty_tpl->getVariable('td_col')->value;?>
"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['value']->value['timestamp'],"%d.%m.%Y - %H:%M:%S");?>
</td>
				<td class="<?php echo $_smarty_tpl->getVariable('td_col')->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['level'];?>
</td>
				<td class="<?php echo $_smarty_tpl->getVariable('td_col')->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['channel'];?>
</td>
				<td class="<?php echo $_smarty_tpl->getVariable('td_col')->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['msg'];?>
</td>
			</tr>
		
			<?php $_smarty_tpl->tpl_vars['change_col'] = new Smarty_variable(($_smarty_tpl->getVariable('change_col')->value+1), null, null);?>
		<?php }?>
	<?php }} ?>
<?php }?>
</table>
<?php }?>