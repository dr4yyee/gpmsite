<?php /* Smarty version Smarty3rc4, created on 2014-08-14 18:58:35
         compiled from "/var/www/httpdocs/ts3wi/templates/bf3/mainbar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19732073953eceabb525086-20962424%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e66d43a75971009c29a503df973348de75037292' => 
    array (
      0 => '/var/www/httpdocs/ts3wi/templates/bf3/mainbar.tpl',
      1 => 1408033904,
    ),
  ),
  'nocache_hash' => '19732073953eceabb525086-20962424',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_smarty_tpl->getVariable('loginstatus')->value===true&&$_smarty_tpl->getVariable('site')->value!=='login'){?>
<td style="width:100px" >
<table style="width:100px" class="border" cellpadding="1" cellspacing="0">
	<tr><td style="width:100px" class="maincat"><?php echo $_smarty_tpl->getVariable('lang')->value['server'];?>
</td></tr>
	<?php if ($_smarty_tpl->getVariable('hoststatus')->value===true||$_smarty_tpl->getVariable('serverhost')->value==false){?>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=server"><?php echo $_smarty_tpl->getVariable('lang')->value['serverlist'];?>
</a></td></tr>
	<?php }?>
	<?php if (!isset($_smarty_tpl->getVariable('port')->value)&&$_smarty_tpl->getVariable('hoststatus')->value===true||!isset($_smarty_tpl->getVariable('port')->value)&&$_smarty_tpl->getVariable('serverhost')->value==false){?>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=createserver"><?php echo $_smarty_tpl->getVariable('lang')->value['createserver'];?>
</a></td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=servertraffic"><?php echo $_smarty_tpl->getVariable('lang')->value['instancetraffic'];?>
</a></td></tr>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=instanceedit"><?php echo $_smarty_tpl->getVariable('lang')->value['instanceedit'];?>
</a></td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=logview"><?php echo $_smarty_tpl->getVariable('lang')->value['logview'];?>
</a></td></tr>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=iserverbackup"><?php echo $_smarty_tpl->getVariable('lang')->value['instancebackup'];?>
</a></td></tr>
		<?php }?>
	<?php if (isset($_smarty_tpl->getVariable('port')->value)){?>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=serverview&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['serverview'];?>
</a></td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=servertraffic&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['virtualtraffic'];?>
</a></td></tr>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=serveredit&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['serveredit'];?>
</a></td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=fileupload&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['iconupload'];?>
</a></td></tr>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=logview&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['logview'];?>
</a></td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=filelist&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['filelist'];?>
</a></td></tr>				
		<tr><td class="green2"><a class="mainbarlink" href="javascript:oeffnefenster('site/interactive.php?port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
&amp;action=action');"><?php echo $_smarty_tpl->getVariable('lang')->value['massaction'];?>
</a></td></tr>
		<tr><td class="maincat"><?php echo $_smarty_tpl->getVariable('lang')->value['channel'];?>
</td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=channel&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['channellist'];?>
</a></td></tr>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=createchannel&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['createchannel'];?>
</a></td></tr>
		<?php if (isset($_smarty_tpl->getVariable('cid')->value)){?>
			<tr><td class="green1"><a class="mainbarlink" href="index.php?site=channelview&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
&amp;cid=<?php echo $_smarty_tpl->getVariable('cid')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['channelview'];?>
</a></td></tr>
			<tr><td class="green2"><a class="mainbarlink" href="index.php?site=channeledit&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
&amp;cid=<?php echo $_smarty_tpl->getVariable('cid')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['channeledit'];?>
</a></td></tr>
		<?php }?>
		<tr><td class="maincat"><?php echo $_smarty_tpl->getVariable('lang')->value['clients'];?>
</td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=counter&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['clientcounter'];?>
</a></td></tr>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=clients&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['clientlist'];?>
</a></td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=complainlist&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['complainlist'];?>
</a></td></tr>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=chanclienteditperm&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['chanclientperms'];?>
</a></td></tr>			
		
		<tr><td class="maincat"><?php echo $_smarty_tpl->getVariable('lang')->value['bans'];?>
</td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=banlist&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['banlist'];?>
</a></td></tr>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=banadd&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['addban'];?>
</a></td></tr>
		
		<tr><td class="maincat"><?php echo $_smarty_tpl->getVariable('lang')->value['groups'];?>
</td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=sgroups&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['servergroups'];?>
</a></td></tr>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=sgroupadd&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['addservergroup'];?>
</a></td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=cgroups&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['channelgroups'];?>
</a></td></tr>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=cgroupadd&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['addchannelgroup'];?>
</a></td></tr>
		
		<tr><td class="maincat"><?php echo $_smarty_tpl->getVariable('lang')->value['token'];?>
</td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=token&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['token'];?>
</a></td></tr>
		
		<tr><td class="maincat"><?php echo $_smarty_tpl->getVariable('lang')->value['backup'];?>
</td></tr>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=backup&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['chanbackups'];?>
</a></td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=serverbackup&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['serverbackups'];?>
</a></td></tr>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=permexport&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['permexport'];?>
</a></td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=clientsexport&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['clientsexport'];?>
</a></td></tr>
		<tr><td class="green2"><a class="mainbarlink" href="index.php?site=bansexport&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['bansexport'];?>
</a></td></tr>
		
		<tr><td class="maincat"><?php echo $_smarty_tpl->getVariable('lang')->value['console'];?>
</td></tr>
		<tr><td class="green1"><a class="mainbarlink" href="index.php?site=console&amp;port=<?php echo $_smarty_tpl->getVariable('port')->value;?>
"><?php echo $_smarty_tpl->getVariable('lang')->value['queryconsole'];?>
</a></td></tr>
		<?php }?>
</table>
</td>
<?php }?>