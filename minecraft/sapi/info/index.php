<?php
$server =  $_GET['server'];
include_once 'MinecraftQuery.class.php'; //include the class
$status = new MinecraftServerStatus(); // call the class

$array = array(
	"1" => 15569,
	"2" => 15561,
	"3" => 15563,
	"4" => 15570,
	"5" => 25565
);

if (array_key_exists($server, $array))
{
	$response = $status->getStatus('127.0.0.1' , $array[$server]);
	if(!$response) {
		echo htmlentities('
		{
		"motd": "offline"
		}
		');
	} else {
		echo htmlentities('
		{
		"motd": "'.$response["motd"].'",
		"version": "'.$response["version"].'",
    		"players": "'.$response["players"].'",
    		"maxplayers": "'.$response["maxplayers"].'"
		}
		');
	}
}
else {
	echo  "unknown";
}
?>
