<!DOCTYPE html>
<head>
    <title>Kontakt | Blox</title>

    <meta charset="ISO-8859-1">
    <meta name="author" content="PlaySteph310">
    <meta name="copyright" content="PlaySteph310">
    <meta name="description" content="Blox - L�uft bei uns! 1.7.2 Minecraft-Server: s.bloxmc.de - Wir bieten zum Bauen eine Freebuild-, SkyBlock- und GS-Welt, dazu eine flache Games- und Kreativwelt. Wir besitzen viele gute Plugins und schreiben auch eigene Features. Jetzt Joinen und Spa� haben!">
    <meta name="keywords" content="blox, bloxmc, bloxler, bloxminecraft, mcservers, voten, 1080pt, hd, youtube, facebook, minecraft, bukkit, spigot, craftbukkit, skript, plugins, slimefun, crackshot">
    <meta name="page-topic" content="Spiel, Minecraft">
    <meta http-equiv="content-language" content="de">
    <meta name="robots" content="index, follow">

    <link href="style.css" type="text/css" rel="stylesheet">
</head>
<body>
<div id="nav">
    <img src="http://image.bloxmc.de/blox.png" id="nav_bild"/>
    <div id="nav_serverinfo">
        <?php include('../server.php');  ?>
    </div>
    <div id="nav_links" align="right">
        <a href="http://bloxmc.de">Home</a>
        <a href="http://forum.bloxmc.de">Forum</a>
        <a href="http://store.bloxmc.de">Store</a>
        <a href="http://bloxmc.de/vote">Voten</a>
        <a href="http://bloxmc.de/infos">Infos</a>
    </div>
</div>

<div id="section">
    <div id="article">
    <h2>Unser Kontaktformular</h2>
    <hr>
    <p>Wir bitten sie dieses Formular nur f&uuml;r sinnvolle Zwecke zu nutzen! Wenn sie uns &uuml;ber E-Mail erreichen m&ouml;chten, so k&ouml;nnen sie die E-Mail '<a href="mailto:admin@bloxmc.de">admin@bloxmc.de</a>' nutzen.</p>
    <form method="post" action="mail.php">
        <br />
        <input type="text" placeholder="Ihr Name" name="name" />
        <br />
        <input type="text" placeholder="Ihre E-Mail" name="mail"/>
        <br />
        <textarea name="text" placeholder="Ihr Text"></textarea>
        <br>
        <br>
        <?php
          require_once('recaptchalib.php');
          $publickey = "6Lf5b_ISAAAAAEbqjPRmw1k-JRU1y_Jie6L0pFFe"; // you got this from the signup page
          echo recaptcha_get_html($publickey);
        ?>
        <br />
        <input type="submit" value="Absenden..."/>
    </form>   
    </div>
</div>

<div id="footer" align="center">
    <p><a href="http://forum.bloxmc.de/index.php?page=LegalNotice">Impressum</a> | <a href="http://bloxmc.de/kontakt"><span>Kontakt</span></a> | <a href="http://forum.bloxmc.de/index.php?page=User&userID=1">Copyright - PlaySteph310</a></p>
</div>
</body>
</html>