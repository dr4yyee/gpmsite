<?php
// Edit this ->
define( 'MQ_SERVER_ADDR', 'gpm-server.de' );
define( 'MQ_SERVER_PORT', 25565 );
define( 'MQ_TIMEOUT', 1 );
// Edit this <-

// Display everything in browser, because some people can't look in logs for errors
Error_Reporting( E_ALL | E_STRICT );
Ini_Set( 'display_errors', true );

require __DIR__ . '/MinecraftQuery.class.php';

$Timer = MicroTime( true );

$Query = new MinecraftQuery( );

try
{
$Query->Connect( MQ_SERVER_ADDR, MQ_SERVER_PORT, MQ_TIMEOUT );
}
catch( MinecraftQueryException $e )
{
$Exception = $e;
}

$Timer = Number_Format( MicroTime( true ) - $Timer, 4, '.', '' );
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Minecraft Query PHP Class</title>
</head>
<body>
<?php if( isset( $Exception ) ): ?>
<div class="panel panel-primary">
<div class="panel-heading"><?php echo htmlspecialchars( $Exception->getMessage( ) ); ?></div>
<p><?php echo nl2br( $e->getTraceAsString(), false ); ?></p>
</div>
<?php else: ?>
<?php if( ( $Info = $Query->GetInfo( ) ) !== false ): ?>
<?php foreach( $Info as $InfoKey => $InfoValue ): ?>
<tr>
<td><?php echo htmlspecialchars( $InfoKey ); ?></td>
<td><?php
if( Is_Array( $InfoValue ) )
{
echo "<pre>";
print_r( $InfoValue );
echo "</pre>";
}
else
{
echo htmlspecialchars( $InfoValue );
}
?></td>
</tr>
<?php endforeach; ?>
<?php else: ?>
<tr>
<td colspan="2">No information received</td>
</tr>
<?php endif; ?>
</tbody>
</table>
</div>
<div class="col-sm-6">
<table class="table table-bordered table-striped">
<thead>
</thead>
<tbody>
</tbody>
</table>
</div>
</div>
<?php endif; ?>
</div>
</body>
</html>