<?php
// Edit this ->
define( 'MQ_SERVER_ADDR', 'gpm-server.de' );
define( 'MQ_SERVER_PORT', 25565 );
define( 'MQ_TIMEOUT', 1 );
// Edit this <-

// Display everything in browser, because some people can't look in logs for errors
//Error_Reporting( E_ALL | E_STRICT );
//Ini_Set( 'display_errors', true );
$socket = fSockOpen(MQ_SERVER_ADDR, MQ_SERVER_PORT, $errNo, $errStr, MQ_TIMEOUT);
if(!$socket || $socket == null) {
    echo "Server nicht erreichbar!";
//    echo "Error: ".$errNo." - ".$errStr;
}
else
{
require __DIR__ . '/MinecraftQuery.class.php';

$Timer = MicroTime( true );

$Query = new MinecraftQuery( );

try
{
$Query->Connect( MQ_SERVER_ADDR, MQ_SERVER_PORT, MQ_TIMEOUT );
}
catch( MinecraftQueryException $e )
{
$Exception = $e;
}

$Timer = Number_Format( MicroTime( true ) - $Timer, 4, '.', '' );
?>
<?php if( isset( $Exception ) ): ?>
<div class="panel panel-primary">
<div class="panel-heading"><?php echo htmlspecialchars( $Exception->getMessage( ) ); ?></div>
<p><?php echo nl2br( $e->getTraceAsString(), false ); ?></p>
</div>
<?php else: ?>
<?php if( ( $Info = $Query->GetInfo( ) ) !== false ): ?>
<?php foreach( $Info as $InfoKey => $InfoValue ): ?>
<?php echo $InfoKey; ?>
<?php
if( Is_Array( $InfoValue ) )
{
echo "<pre>";
print_r( $InfoValue );
echo "</pre>";
}
else
{
echo $InfoValue;
}
?>
<?php endforeach; ?>
<?php else: ?>
<?php endif; ?>
<?php endif; ?>
<?php } ?>