<!DOCTYPE html>
<html>

<head>
    <title>&gt; GPM-Server  &lt;</title>

    <meta charset="UTF-8">
	<meta name="author" content="PlaySteph310">
	<meta name="copyright" content="PlaySteph310">
	<meta name="description" content="GPM-Server reloaded! Trete unserer Community bei und erlebe neuen Spielspa� wie DU ihn nicht kennst!">
	<meta name="keywords" content="blox, minecraft, server, 1.7.7, neu, bukkit, spigot, plugins, lagfrei, lustig, freebuild, games, events, gs, grundst�ck, �berleben, survival, kreativ, creative, vote, nocheatplus, lwc, cool, freistadt, gpm-server, gpm, join, now, build, pvp, games, lobby, immer online, selbstgeschrieben plugins, erfahrenes team, 1.7.10, 1.8, komlett, skyblock, creative">
<?php
function check_mobile() {
  $agents = array(
    'Windows CE', 'Pocket', 'Mobile',
    'Portable', 'Smartphone', 'SDA',
    'PDA', 'Handheld', 'Symbian',
    'WAP', 'Palm', 'Avantgo',
    'cHTML', 'BlackBerry', 'Opera Mini',
    'Nokia', 'Android'
  );

  // Pr�fen der Browserkennung
  for ($i=0; $i<count($agents); $i++) {
    if(isset($_SERVER["HTTP_USER_AGENT"]) && strpos($_SERVER["HTTP_USER_AGENT"], $agents[$i]) !== false)
      return true;
  }

  return false;
}

$style = '';

if(check_mobile())
{
    $style = 'handheld.css';
    ?>
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <?php
}
else
{
    $style = 'style.css';
}
 ?>
     <link href="<?php echo $style ?>" type="text/css" rel="stylesheet">

</head>
<body>
    <div align="center" id="text">
            <h1>Besondere Features</h1>
        <div align="justify" id="section">
            <ul>
                <li>Leistungsstarker Root mit 48GB RAM (32GB reell und 16GB SWAP) und einem Intel i7-Prozessor</li>
                <li>Jahrelange Erfahrung im Gameserverhosting</li>
                <li>2 Server:</li>
                    <ul>
                        <li>Buildserver - Verschiedenste Arten von Freebuild</li>
                        <li>Gameserver - Minigames ohne Ende!</li>
                    </ul>
                <li>TeamSpeak3 Server - <a href="ts3server://gpm-server.de">gpm-server.de</a></li>
                <li>Plugins:</li>
                    <ul>
                        <li>Selbstgeschriebene Features</li>
                        <li>Nachgeahmte Mods wie InventoryTweaks</li>
                        <li>Chunkkaufsystem - Kaufe dir f&uuml;r g&uuml;nstige Preise eigene Chunks auf dem Buildserver</li>
                        <li>Umfragen - Entscheidet selbst &uuml;ber Tag und Nacht</li>
                        <li>Mobspawner - Baue sie ab und &auml;ndere ihren Mob</li>
                        <li>Wirtschaft - Erfarme dir durch Jobs Cookies, Kaufe und Verkaufe Items durch Chestshops</li>
                    </ul>
            </ul>
        </div>
    </div>
    <div align="center">
        <div id="links" align="justify">
            <a href="javascript:history.back();"><div align="center">Zur&uuml;ck</div></a>
        </div>
    </div>
</body>
</html>
